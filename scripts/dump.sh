#!/bin/bash

#-----------------------------------------------------------

function dump () {
    local PREFIX='data/backups'
    local SUFFIX="$(date +%Y-%m-%d_%H-%M-%S)-pretty.json"

    python manage.py dumpdata \
    "$1" \
    --indent 2 --format json --output \
    "$PREFIX"/"$2"-"$SUFFIX"
}

#-----------------------------------------------------------

dump '--all' 'all'
dump 'a0_aux.customuser' 'users'
dump 'blog' 'blog'
