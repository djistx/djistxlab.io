#!/bin/bash

source scripts/__dry.sh

# ----------------------------------------------------------

function load () {
    local PREFIX='data/seeds'
    local SUFFIX="json"

    # maybe this should be separate
    # python manage.py migrate

    python manage.py loaddata \
    "$PREFIX"/"$1"."$SUFFIX"
}

#-----------------------------------------------------------

function select_seed () {
    printf 'Select seed to load (default=Quit)\n'

    local options=(
      'all'
      'users'
      'blog'
      'QUIT'
    )

    # this helper function is in scripts/__dry.sh
    local opt=$(__select_with_default "${options[@]}")

    case $opt in
      'all')   load 'all';;
      'users') load 'users';;
      'blog')  load 'blog';;
      ''|'QUIT') echo 'Bye!';;
    esac
}

#-----------------------------------------------------------

__print_separator

select_seed
