#!/bin/bash

# ----------------------------------------------------------

function __print_separator () {
    printf '\n------------------------------------------------------------\n'
}

# ----------------------------------------------------------

# https://stackoverflow.com/questions/42789273/bash-choose-default-from-case-when-enter-is-pressed-in-a-select-prompt

function __select_with_default () {

  local item i=0 numItems=$# 

  # Print numbered menu items, based on the arguments passed.

  # Short for: for item in "$@"; do
  for item; do
    printf '%s\n' "$((++i))) $item"
  done >&2 # Print to stderr, as `select` does.

  # Prompt the user for the index of the desired item.
  while :; do
    printf %s "${PS3-#? }" >&2 # Print the prompt string to stderr, as `select` does.
    read -r index
    # Make sure that the input is either empty or that a valid index was entered.
    [[ -z $index ]] && break  # empty input
    (( index >= 1 && index <= numItems )) 2>/dev/null || { echo "Invalid selection. Please try again." >&2; continue; }
    break
  done

  # Output the selected item, if any.
  [[ -n $index ]] && printf %s "${@: index:1}"

}
