#!/bin/bash

source scripts/__dry.sh

# ----------------------------------------------------------

function pg_create () {
    createdb --echo --username=postgres "$1"
    python manage.py migrate
    bash scripts/load.sh
    psql --echo-all --echo-errors --command='\dt' --dbname="$1" postgres
    psql --echo-all --echo-errors --command='SELECT * FROM a0_aux_customuser;' --dbname="$1" postgres
}

# ----------------------------------------------------------

function pg_drop () {
    dropdb --interactive --echo --username=postgres "$1"
    psql --echo-all --echo-errors --command='\l' postgres
}

#-----------------------------------------------------------

function pg_tasks () {
    printf 'Select postgres task to perform (default=Quit)\n'

    local options=(
      'pg create djistx'
      'pg drop djistx'
      'QUIT'
    )

    # this helper function is in scripts/__dry.sh
    local opt=$(__select_with_default "${options[@]}")

    case $opt in
      'pg create djistx') pg_create 'djistx';;
      'pg drop djistx')   pg_drop 'djistx';;
      ''|'QUIT') echo 'Bye!';;
    esac
}

#-----------------------------------------------------------

__print_separator

pg_tasks
