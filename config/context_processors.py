import platform
from importlib.metadata import version
from django.utils import timezone as aware
from django.conf import settings


def dynamic(request):
    return {
        # https://docs.djangoproject.com/en/stable/topics/i18n/timezones/
        # https://strftime.org/
        'CURRENT_AWARE_DATETIME': aware.now().strftime('%a %d %b %Y at %H:%M:%S %Z %z (%s Unix)'),

        # https://note.nkmk.me/en/python-sys-platform-version-info/
        # https://itsmycode.com/how-to-check-and-print-python-version/
        # https://stackoverflow.com/questions/20180543/how-to-check-the-version-of-python-modules
        'PYTHON_VERSION_PLATFORM': platform.python_version(),
        'DJANGO_VERSION_IMPORTLIB': version('django'),
        'DISTILL_VERSION_IMPORTLIB': version('django_distill'),
    }

#===========================================================

def from_settings(request):
    return {

        # set in config/settings/base_import/frontend.py
        'ACTIVE_THEME_NAME': settings.ACTIVE_THEME_NAME,
        'SITE_TITLE': settings.SITE_TITLE,

        # set in, e.g., config/settings/dev.py
        'DEBUG': settings.DEBUG,
        'CURRENT_ENV_DISPLAYED': settings.CURRENT_ENV_DISPLAYED,
    }

#===========================================================

'''
These items could eventually be pulled in from settings,
if there is sufficient reason to justify the duplication
'''

def proto_settings(request):
    return {

        # Google Analytics
        # SITE_GA4_ID = '' # add related partial to templates

        'SITE_GENERATOR_META_ENABLED': True,
        'SITE_INFO_FOOTER_ENABLED': True,

        # https://www.w3.org/TR/WCAG20-TECHS/H33.html

        'SITE_REPO_LINK_URL': 'https://gitlab.com/djistx/djistx.gitlab.io/',
        'SITE_REPO_LINK_TEXT': 'Repo',
        'SITE_REPO_LINK_TARGET': '_blank',
        'SITE_REPO_LINK_TITLE': 'Link to the demo site source code repository',

        'SITE_DEMO_LINK_URL': 'https://djistx.gitlab.io/',
        'SITE_DEMO_LINK_TEXT': 'Demo',
        'SITE_DEMO_LINK_TARGET': '_blank',
        'SITE_DEMO_LINK_TITLE': 'Link to the live demo site',

        'SITE_CI_LINK_URL': 'https://gitlab.com/djistx/djistx.gitlab.io/-/pipelines',
        'SITE_CI_LINK_TEXT': 'Pipelines',
        'SITE_CI_LINK_TARGET': '_blank',
        'SITE_CI_LINK_TITLE': 'Link to the demo site CI/CD pipelines page',

        # this URL will change when you re-test
        'SITE_GTM_LINK_URL': 'https://gtmetrix.com/reports/djistx.gitlab.io/3F1LC3Yr/',
        'SITE_GTM_LINK_TEXT': 'GTmetrix',
        'SITE_GTM_LINK_TARGET': '_blank',
        'SITE_GTM_LINK_TITLE': 'Link to the latest GTmetrix performance report for this site',

        # BuiltWith does not respect robots.txt, even though they give you
        # instructions for how block their crawler, so just embrace it
        'SITE_BW_LINK_URL': 'https://builtwith.com/?https%3a%2f%2fdjistx.gitlab.io%2f',
        'SITE_BW_LINK_TEXT': 'BuiltWith',
        'SITE_BW_LINK_TARGET': '_blank',
        'SITE_BW_LINK_TITLE': 'Link to the BuiltWith profile for this site',

    }
