from django.contrib import admin
from django.urls import path, include
from django.conf import settings

#-----------------------------------------------------------
# ADMIN
#-----------------------------------------------------------

admin.site.site_title  = settings.SITE_TITLE
admin.site.index_title = "Admin Home"
admin.site.site_header = settings.SITE_TITLE # + " admin dashboard"  

urlpatterns = [
    path('admin/', admin.site.urls),
]

#-----------------------------------------------------------
# ERROR PAGES
#-----------------------------------------------------------

from django.conf.urls import handler404
handler404 = 'config.views_other.error_404'


#-----------------------------------------------------------
# OTHER ITEMS
#-----------------------------------------------------------

from django.views import defaults as default_views
if settings.DEBUG:
    
    # This allows the error pages to be debugged during development, just visit
    # these url in browser to see how these error pages look like.

    urlpatterns += [
        path(
            "404/",
            default_views.page_not_found,
            kwargs={"exception": Exception("Page not Found")},
        ),
    ]

    #-------------------------------------------------------
    # DEBUG TOOLBAR
    #-------------------------------------------------------
    if "debug_toolbar" in settings.INSTALLED_APPS:

        import debug_toolbar

        urlpatterns = [
            path("__debug__/", include(debug_toolbar.urls))
        ] + urlpatterns
