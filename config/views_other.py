from django.conf import settings
from django.http import FileResponse, HttpRequest, HttpResponse
from django.views.decorators.cache import cache_control
from django.views.decorators.http import require_GET
from django.shortcuts import render

#-----------------------------------------------------------
# ROBOTS
#-----------------------------------------------------------

"""
@require_GET
@cache_control(max_age=60 * 60 * 24, immutable=True, public=True)  # one day
def robots_file(request: HttpRequest) -> HttpResponse:
    file = (settings.FE_BASE_DIR / 'frontend/static_unversioned/robots.ini').open("rb")
    return FileResponse(file)
"""

@require_GET
@cache_control(max_age=60 * 60 * 24, immutable=True, public=True)  # one day
def robots_file_rename(request: HttpRequest) -> HttpResponse:
    file = (settings.THEMES_BASE_DIR / 'static_unversioned/robots.txt.np.ini').open("rb")
    
    response = FileResponse(file)
    response['Content-Disposition'] = 'inline; filename="robots.txt"'
    response['Content-Type'] = 'text/plain'
    
    return response

#-----------------------------------------------------------
# CUSTOM ERROR PAGES
#-----------------------------------------------------------

def error_404(request, exception):
    return render(request, '404.html')
