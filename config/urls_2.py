from django_distill import distill_path
# from config.views_other import robots_file_rename

#-----------------------------------------------------------
# ROBOTS.TXT
#-----------------------------------------------------------

# https://ordinarycoders.com/blog/article/robots-text-file-django


def get_robots():
    return None

urlpatterns = [

    distill_path('robots.txt',
        robots_file_rename,
        name='robots_dot_txt',
        distill_func=get_robots
        # distill_file='robots.txt'
    ),

]

#-----------------------------------------------------------
# FAVICONS
#-----------------------------------------------------------
