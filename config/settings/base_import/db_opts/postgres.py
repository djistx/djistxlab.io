# https://docs.djangoproject.com/en/stable/ref/settings/#databases

import dj_database_url

DATABASE_URL = 'postgres://postgres:postgres@127.0.0.1:5432/djistx'
DATABASES = {'default': dj_database_url.parse(DATABASE_URL)}
