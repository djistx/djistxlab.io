from pathlib import Path

DB_BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent.parent

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': DB_BASE_DIR / 'data/relations.sqlite3',
    }
}
