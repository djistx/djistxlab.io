"""
comment the sqlite import and uncomment the postgres import
if you prefer to use postgres locally, and have a password
configured specifically for user `postgres`

then run the following scripts from the root of this repo:
./scripts/pg.sh
./scripts/load.sh
"""

from .db_opts.sqlite import *
# from .db_opts.postgres import *

#-----------------------------------------------------------

# https://docs.djangoproject.com/en/stable/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project
# https://docs.djangoproject.com/en/stable/ref/settings/#auth-user-model
AUTH_USER_MODEL = "a0_aux.CustomUser"

# https://docs.djangoproject.com/en/stable/ref/settings/#std:setting-DEFAULT_AUTO_FIELD
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'
