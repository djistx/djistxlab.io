INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles', # required by debug_toolbar

    #-------------------------------------------------------
    # MORE THIRD-PARTY APPS
    #-------------------------------------------------------

    'compressor',
    # 'django_cachekiller',

    'django_distill',

    #-------------------------------------------------------
    # MY PRIMARY APPS
    #-------------------------------------------------------

    'apps.a0_aux.apps.A0AuxConfig',
    'apps.blog',
]

#===========================================================

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',

    #-------------------------------------------------------
    # MORE THIRD-PARTY MIDDLEWARE
    #-------------------------------------------------------

    # https://github.com/cobrateam/django-htmlmin
    'htmlmin.middleware.HtmlMinifyMiddleware',
    'htmlmin.middleware.MarkRequestMiddleware',
]
