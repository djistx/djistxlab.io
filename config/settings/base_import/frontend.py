from pathlib import Path


SITE_TITLE = 'djistx' # used in admin and themes

#-----------------------------------------------------------
# THEMES SUPPORT
#-----------------------------------------------------------

ACTIVE_THEME_NAME = 'Doodling DTL'

ACTIVE_THEME_SLUG = 'doodling_dtl'

THEMES_BASE_DIR = Path(__file__).resolve().parent.parent.parent.parent

THEMES_DIR = THEMES_BASE_DIR / 'themes'

#-----------------------------------------------------------
# TEMPLATING
#-----------------------------------------------------------

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',

        'DIRS': [
            THEMES_DIR / ACTIVE_THEME_SLUG / 'templates',
        ],

        'APP_DIRS': True, # required by debug_toolbar

        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',

                # project custom context processors
                'config.context_processors.dynamic',
                'config.context_processors.from_settings',
                'config.context_processors.proto_settings',
            ],
        },
    },
]

#-----------------------------------------------------------
# HTML MINIFICATION
#-----------------------------------------------------------

# https://github.com/cobrateam/django-htmlmin

# HTML_MINIFY = True # using a `prod` settings variant now

# KEEP_COMMENTS_ON_MINIFYING = True
# CONSERVATIVE_WHITESPACE_ON_MINIFYING = True
# EXCLUDE_FROM_MINIFYING = ('^my_app/', '^admin/')

#-----------------------------------------------------------
# STATIC FILE HANDLING
#-----------------------------------------------------------

# https://docs.djangoproject.com/en/stable/howto/static-files/

STATIC_URL = 'static/'

STATICFILES_DIRS = [
    THEMES_DIR / ACTIVE_THEME_SLUG / 'static',
]

# https://docs.djangoproject.com/en/stable/ref/settings/#static-root
STATIC_ROOT = str(THEMES_BASE_DIR / 'pub_static')

STATICFILES_FINDERS = (
    # default
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',

    # other finders
    'compressor.finders.CompressorFinder',
)

COMPRESS_ENABLED = True

#-----------------------------------------------------------
# DISTILL
#-----------------------------------------------------------

DISTILL_DIR = str(THEMES_BASE_DIR / 'public')
