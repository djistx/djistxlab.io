# https://discuss.dizzycoding.com/what-does-noqa-mean-in-python-comments/

from .base_import.apps_and_middleware import *
from .base_import.database import *
from .base_import.frontend import *

#-----------------------------------------------------------

ROOT_URLCONF = 'config.urls_0'

LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

AUTH_PASSWORD_VALIDATORS = [
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
]

#-----------------------------------------------------------

# in case you use runserver for more than one project simultaneously
CSRF_COOKIE_NAME = 'csrf_cookie_for_djistx'

# presumably these do not really matter for a static site
WSGI_APPLICATION = 'config.wsgi.application'
SECRET_KEY = 'some-secret-key'
