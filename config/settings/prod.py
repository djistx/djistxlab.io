from .base import *

# https://docs.djangoproject.com/en/stable/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['*']
DEBUG = False
CURRENT_ENV_DISPLAYED = ''
