from .base import *

# https://docs.djangoproject.com/en/stable/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost', '0.0.0.0', '127.0.0.1']
DEBUG = True
CURRENT_ENV_DISPLAYED = 'DEV'

# ----------------------------------------------------------
# DJANGO DEBUG TOOLBAR
# ----------------------------------------------------------

# this is borrowed from Cookiecutter Django

# https://github.com/jazzband/django-debug-toolbar

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#internal-ips
INTERNAL_IPS = ['127.0.0.1', '10.0.2.2']

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#prerequisites
INSTALLED_APPS += ['debug_toolbar']  # noqa F405

# https://django-debug-toolbar.readthedocs.io/en/latest/installation.html#middleware
# order is important (see docs)
MIDDLEWARE += ['debug_toolbar.middleware.DebugToolbarMiddleware']  # noqa F405

# https://django-debug-toolbar.readthedocs.io/en/latest/configuration.html#debug-toolbar-config
DEBUG_TOOLBAR_CONFIG = {
    'DISABLE_PANELS': ['debug_toolbar.panels.redirects.RedirectsPanel'],
    'SHOW_TEMPLATE_CONTEXT': True,
}
