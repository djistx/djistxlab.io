from .base import *

# https://docs.djangoproject.com/en/stable/ref/settings/#allowed-hosts
ALLOWED_HOSTS = ['localhost', '0.0.0.0', '127.0.0.1']
DEBUG = False
CURRENT_ENV_DISPLAYED = 'STAGING'
