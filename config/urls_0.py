"""
https://docs.djangoproject.com/en/stable/topics/http/urls/
"""

from django.urls import path, include

#-----------------------------------------------------------
# APPS
#-----------------------------------------------------------

urlpatterns = [
    path('', include('apps.blog.urls')),
]

#-----------------------------------------------------------
# ROOT URLCONF ADDENDA
#-----------------------------------------------------------

urlpatterns = [

    # admin, error pages, debug toolbar
    path("", include("config.urls_1")),

    # robots.txt, favicons
    # path("", include("config.urls_2")),

] + urlpatterns
