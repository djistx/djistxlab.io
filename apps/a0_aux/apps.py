from django.apps import AppConfig


class A0AuxConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.a0_aux'
    verbose_name = 'a0_aux: Abstract User Model'
