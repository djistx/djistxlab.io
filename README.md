# django-distill-example

This is based on [django-distill-example](https://github.com/meeb/django-distill-example)

My version uses an abstract user model (among other differences from the upstream example)

The admin username is now `admin` and the password is now `admin`
