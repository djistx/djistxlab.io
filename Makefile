python=/usr/bin/env python

all: clean build

build:
	mkdir -p pub_static
	mkdir -p public
	
	$(python) manage.py collectstatic --noinput \
	--settings config.settings.prod
	--ignore admin \
	--ignore theme --ignore blog \

	
	$(python) manage.py distill-local --force \
	--settings config.settings.prod

clean:
	rm -rf public
	rm -rf pub_static
